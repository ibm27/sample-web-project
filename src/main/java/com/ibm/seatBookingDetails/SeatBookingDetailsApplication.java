package com.ibm.seatBookingDetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.ibm.seatBookingDetails")
public class SeatBookingDetailsApplication {
	public static void main(String[] args) {
		SpringApplication.run(SeatBookingDetailsApplication.class, args);
	}

}
