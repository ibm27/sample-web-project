package com.ibm.seatBookingDetails.service;

import java.util.List;

import com.ibm.seatBookingDetails.domain.SeatBookingDetails;

public interface SeatBookingDetailsService {
	 //Saving office details
    public SeatBookingDetails saveSeatBookingDetails(SeatBookingDetails seatBookingDetails);
    //update office details
    SeatBookingDetails updateSeatBookingDetails(SeatBookingDetails seatBookingDetails, int seatId);
    // List of available seat based on the search by city name and office Id
    List<SeatBookingDetails> getAllAvailableBookingDetailsByCityNameOfficeId(String cityName,String officeId);  
    // List of booked seat based on the search by city name and office Id
    List<SeatBookingDetails> getAllBookedSeatDetailsByCityNameOfficeId(String cityName,String officeId);
    //Book Seats
    SeatBookingDetails bookSeats(int seatId);
    //Unbook Seats
    SeatBookingDetails unbookSeats(int seatId);
    
    
}
